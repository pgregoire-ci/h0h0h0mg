
FROM debian:latest

RUN apt-get update && apt-get -y install socat netcat-openbsd procps

RUN useradd -s/bin/bash -m u
COPY service.sh /service.sh
RUN sh -c 'echo root:root|chpasswd'

USER u
HEALTHCHECK CMD /bin/true
CMD ["/bin/bash", "/service.sh"]
